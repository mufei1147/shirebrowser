# shirebrowser

#### 介绍
shirebrowser是一款湿热一瞬间的第三方辅助应用，专注于便捷浏览。应用不会收集任何用户数据与信息。浏览器没办法上湿热?网络不支持?这些都不是问题啦。湿热便捷浏览器，专为浏览湿热而量身打造，方便快捷稳定流畅。

#### 软件架构
软件架构说明


#### 安装教程

下载地址：[https://gitee.com/mufei1147/shirebrowser/releases](https://gitee.com/mufei1147/shirebrowser/releases)
1.  下载对应的应用安装包
2.  打开并安装

#### 使用说明

无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
